#ifndef FLYWHEEL_H
#define FLYWHEEL_H

#include "Motor.h"
#include "InputDevice.h"
#include "IMU.h"

class FlywheelMotor : public Motor {

public:
    FlywheelMotor();
    ~FlywheelMotor();
    void driveMotor(InputDevice* inputDevice, IMU imu);
    void enableMotor();
    void disableMotor();

private:
    const byte PIN_FLYWHEEL_R_PWM = 6;
    const byte PIN_FLYWHEEL_L_PWM = 7;

    const byte PIN_FLYWHEEL_R_EN = 44;
    const byte PIN_FLYWHEEL_L_EN = 45;

};

#endif
