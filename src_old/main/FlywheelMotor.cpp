#include "FlywheelMotor.h"

FlywheelMotor::FlywheelMotor() {
    pinMode(PIN_FLYWHEEL_R_EN, OUTPUT);
    pinMode(PIN_FLYWHEEL_L_EN, OUTPUT);
}

FlywheelMotor::~FlywheelMotor() {}

void FlywheelMotor::driveMotor(InputDevice* inputDevice, IMU imu) {
    if(!motorEnabled) {
        analogWrite(PIN_FLYWHEEL_R_PWM, 0);
        analogWrite(PIN_FLYWHEEL_L_PWM, 0);
        return;
    }

    short input = inputDevice->getFlywheelInput();
    if(input > 0) {
        analogWrite(PIN_FLYWHEEL_R_PWM, input);
        analogWrite(PIN_FLYWHEEL_L_PWM, 0);
    } else if(input < 0) {
        analogWrite(PIN_FLYWHEEL_R_PWM, 0);
        analogWrite(PIN_FLYWHEEL_L_PWM, abs(input));
    } else {
        analogWrite(PIN_FLYWHEEL_R_PWM, 0);
        analogWrite(PIN_FLYWHEEL_L_PWM, 0);
    }
}

void FlywheelMotor::enableMotor() {
    Motor::enableMotor();
    digitalWrite(PIN_FLYWHEEL_R_EN, HIGH);
    digitalWrite(PIN_FLYWHEEL_L_EN, HIGH);
}

void FlywheelMotor::disableMotor() {
    Motor::disableMotor();
    digitalWrite(PIN_FLYWHEEL_R_EN, LOW);
    digitalWrite(PIN_FLYWHEEL_L_EN, LOW);
}
