#include "MainDriveMotor.h"

MainDriveMotor::MainDriveMotor() {
    pinMode(PIN_MAINDRIVE_R_EN, OUTPUT);
    pinMode(PIN_MAINDRIVE_L_EN, OUTPUT);

    drivePID.SetMode(AUTOMATIC);
    drivePID.SetOutputLimits(-255, 255);
    drivePID.SetSampleTime(100);
}

MainDriveMotor::~MainDriveMotor() {}

void MainDriveMotor::driveMotor(InputDevice* inputDevice, IMU imu) {
//   Serial.print("pitch: ");
//   Serial.print(imu.getPitch());
//   Serial.print("\t");
//   Serial.print("setpoint: ");
//   Serial.print(inputDevice->getThrottleInput());
//   Serial.print("\t");
//   Serial.print("output: ");
//   Serial.print(driveOutput);
//   Serial.print("\t");
//   Serial.print("pwm: ");
//   Serial.println(map(abs(driveOutput), 0, 255, MIN_PWM, 255));
    
    if(!motorEnabled) {
        analogWrite(PIN_MAINDRIVE_R_PWM, 0);
        analogWrite(PIN_MAINDRIVE_L_PWM, 0);
        return;
    }

    driveInput = imu.getPitch();
//    driveSetpoint = inputDevice->getThrottleInput();
//    driveSetpoint = map(driveSetpoint, -255, 255, 50, -50);
    driveSetpoint = 0;
    drivePID.Compute();

    if(driveOutput > 0) {
        analogWrite(PIN_MAINDRIVE_R_PWM, map(driveOutput, 0, 255, MIN_PWM, 255));
        analogWrite(PIN_MAINDRIVE_L_PWM, 0);
    } else if(driveOutput < 0) {
        analogWrite(PIN_MAINDRIVE_R_PWM, 0);
        analogWrite(PIN_MAINDRIVE_L_PWM, map(abs(driveOutput), 0, 255, MIN_PWM, 255));
    } else {
        analogWrite(PIN_MAINDRIVE_R_PWM, 0);
        analogWrite(PIN_MAINDRIVE_L_PWM, 0);
    }
}

void MainDriveMotor::enableMotor() {
    Motor::enableMotor();
    digitalWrite(PIN_MAINDRIVE_R_EN, HIGH);
    digitalWrite(PIN_MAINDRIVE_L_EN, HIGH);
    drivePID.SetMode(AUTOMATIC);
}

void MainDriveMotor::disableMotor() {
    Motor::disableMotor();
    digitalWrite(PIN_MAINDRIVE_R_EN, LOW);
    digitalWrite(PIN_MAINDRIVE_L_EN, LOW);
    drivePID.SetMode(MANUAL);
    driveOutput = 0;
}
