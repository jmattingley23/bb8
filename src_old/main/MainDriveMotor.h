#ifndef MAIN_DRIVE_MOTOR_H
#define MAIN_DRIVE_MOTOR_H

#include "Motor.h"
#include "InputDevice.h"
#include "IMU.h"

#include <PID_v1.h>

class MainDriveMotor : public Motor {

public:
    MainDriveMotor();
    ~MainDriveMotor();
    void driveMotor(InputDevice* inputDevice, IMU imu);
    void enableMotor();
    void disableMotor();

private:
    const byte PIN_MAINDRIVE_R_PWM = 8;
    const byte PIN_MAINDRIVE_L_PWM = 9;

    const byte PIN_MAINDRIVE_R_EN = 48;
    const byte PIN_MAINDRIVE_L_EN = 49;

    const byte MIN_PWM = 55;

    double driveInput = 0;
    double driveOutput = 0;
    double driveSetpoint = 0;
    PID drivePID{&driveInput, &driveOutput, &driveSetpoint, 5.0, 0.2, 0.1, REVERSE};
};

#endif
