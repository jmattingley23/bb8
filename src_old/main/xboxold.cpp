#include "XboxController.h"

USB usb;
XBOXRECV xboxRecv(&usb);

XboxController::XboxController(Motor* motors[], byte numMotors) : InputDevice(motors, numMotors) {
    if (usb.Init() == -1) {
        Serial.println(F("OSC did not start"));
        while (1); //halt
    }
    Serial.println(F("Xbox Wireless Receiver Library Started"));
}

XboxController::~XboxController() {}

void XboxController::gatherAllInputs() {
    usb.Task();
    if(!xboxRecv.XboxReceiverConnected) {
        disableAllMotors();
        return;
    }
    if(!xboxRecv.Xbox360Connected[0]) {
        disableAllMotors();
        return;
    }

    if(xboxRecv.getButtonClick(START, 0)) {
        enableAllMotors();
    }
    if(xboxRecv.getButtonClick(BACK, 0)) {
        disableAllMotors();
    }

    getTriggerInputs();
    getLeftStickXInput();
    getLeftStickYInput();
    getRightStickXInput();
    getRightStickYInput();
    getBumperInputs();
}

void XboxController::getTriggerInputs() {
    rightTriggerPosition = xboxRecv.getButtonPress(R2, 0);
    if(rightTriggerPosition < XBOX_TRIGGER_THRESHOLD) {
        rightTriggerPosition = 0;
    }
    leftTriggerPosition = xboxRecv.getButtonPress(L2, 0);
    if(leftTriggerPosition < XBOX_TRIGGER_THRESHOLD) {
        leftTriggerPosition = 0;
    }
    leftTriggerPosition = -leftTriggerPosition;
    throttleInput = rightTriggerPosition + leftTriggerPosition;
}

void XboxController::getLeftStickXInput() {
    leftStickPositionX = xboxRecv.getAnalogHat(LeftHatX, 0);
    if(leftStickPositionX > -XBOX_STICK_THRESHOLD && leftStickPositionX < XBOX_STICK_THRESHOLD) {
        leftStickPositionX = 0;
    }
    sideAxisInput = map(leftStickPositionX, XBOX_STICK_MIN, XBOX_STICK_MAX, -255, 255);
}

void XboxController::getLeftStickYInput() {
    leftStickPositionY = xboxRecv.getAnalogHat(LeftHatY, 0);
    if(leftStickPositionY > -XBOX_STICK_THRESHOLD && leftStickPositionY < XBOX_STICK_THRESHOLD) {
        leftStickPositionY = 0;
    }
    flywheelInput = map(leftStickPositionY, XBOX_STICK_MIN, XBOX_STICK_MAX, -255, 255);
}

void XboxController::getRightStickXInput() {
    rightStickPositionX = xboxRecv.getAnalogHat(RightHatX);
    if(rightStickPositionX > -XBOX_STICK_THRESHOLD && rightStickPositionX < XBOX_STICK_THRESHOLD) {
        rightStickPositionX = 0;
    }
    headSideInput = map(rightStickPositionX, XBOX_STICK_MIN, XBOX_STICK_MAX, -255, 255);
}

void XboxController::getRightStickYInput() {
    rightStickPositionY = xboxRecv.getAnalogHat(RightHatY, 0);
    if(rightStickPositionY > -XBOX_STICK_THRESHOLD && rightStickPositionY < XBOX_STICK_THRESHOLD) {
        rightStickPositionY = 0;
    }
    headForwardInput = map(rightStickPositionY, XBOX_STICK_MIN, XBOX_STICK_MAX, -255, 255);
}

void XboxController::getBumperInputs() {
    if(xboxRecv.getButtonPress(R1, 0)) {
        headSpinInput = 255;
    } else if(xboxRecv.getButtonPress(L1, 0)) {
        headSpinInput = -255;
    } else {
        headSpinInput = 0;
    }
}
