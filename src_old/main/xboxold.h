#ifndef XBOX_CONTROLLER_H
#define XBOX_CONTROLLER_H

#include "InputDevice.h"
#include "Motor.h"

#include <Arduino.h>
#include <XBOXRECV.h>
#include <SPI.h>

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
    #include <spi4teensy3.h>
#endif

class XboxController : public InputDevice {

public:
    XboxController(Motor* motors[], byte numMotors);
    ~XboxController();
    void gatherAllInputs();
  
private:
    void getTriggerInputs();
    void getLeftStickXInput();
    void getLeftStickYInput();
    void getRightStickXInput();
    void getRightStickYInput();
    void getBumperInputs();

    const int XBOX_STICK_MAX = 32767;
    const int XBOX_STICK_MIN = -32768;
    const int XBOX_STICK_THRESHOLD = 7500;
    const int XBOX_TRIGGER_THRESHOLD = 40;

    int rightTriggerPosition = 0;
    int leftTriggerPosition = 0;
    int leftStickPositionX = 0;
    int leftStickPositionY = 0;
    int rightStickPositionX = 0;
    int rightStickPositionY = 0;
};

#endif
