#include "SideAxisMotor.h"

SideAxisMotor::SideAxisMotor() {
    pinMode(PIN_SIDE_R_EN, OUTPUT);
    pinMode(PIN_SIDE_L_EN, OUTPUT);

    sidePID.SetMode(AUTOMATIC);
    sidePID.SetOutputLimits(-255, 255);
    sidePID.SetSampleTime(20);

    stabilityPID.SetMode(AUTOMATIC);
    stabilityPID.SetOutputLimits(-255, 255);
    stabilityPID.SetSampleTime(20);
}

SideAxisMotor::~SideAxisMotor() {}

void SideAxisMotor::driveMotor(InputDevice* inputDevice, IMU imu) {
    // Serial.print("roll: ");
    // Serial.print(imu.getRoll());
    // Serial.print("\t");
    // Serial.print("stick: ");
    // Serial.print(inputDevice->getSideAxisInput());
    // Serial.print("\t");
    // Serial.print("pwm: ");
    // Serial.println(sideOutput);
    if(!motorEnabled) {
        analogWrite(PIN_SIDE_R_PWM, 0);
        analogWrite(PIN_SIDE_L_PWM, 0);
        return;
    }

    stabilityInput = imu.getRoll();
    stabilitySetpoint = 0;
//    stabilitySetpoint = inputDevice->getSideAxisInput();
    stabilityPID.Compute();

    sideInput = analogRead(PIN_SIDE_POT);
    sideInput = constrain(sideInput, SIDE_POT_MIN, SIDE_POT_MAX);
    sideInput = map(sideInput, SIDE_POT_MIN, SIDE_POT_MAX, -255, 255);
    sideSetpoint = stabilityOutput;
    sidePID.Compute();

    // if(sideOutput > 0) {
    //      analogWrite(PIN_SIDE_R_PWM, map(sideOutput, 0, 255, MIN_PWM, 255));
    //      analogWrite(PIN_SIDE_L_PWM, 0);
    // } else if(sideOutput < 0) {
    //      analogWrite(PIN_SIDE_R_PWM, 0);
    //      analogWrite(PIN_SIDE_L_PWM, map(abs(sideOutput), 0, 255, MIN_PWM, 255));
    // } else {
    //      analogWrite(PIN_SIDE_R_PWM, 0);
    //      analogWrite(PIN_SIDE_L_PWM, 0);
    // }

}

void SideAxisMotor::enableMotor() {
    Motor::enableMotor();
    digitalWrite(PIN_SIDE_R_EN, HIGH);
    digitalWrite(PIN_SIDE_L_EN, HIGH);
    sidePID.SetMode(AUTOMATIC);
    stabilityPID.SetMode(AUTOMATIC);
}

void SideAxisMotor::disableMotor() {
    Motor::disableMotor();
    digitalWrite(PIN_SIDE_R_EN, LOW);
    digitalWrite(PIN_SIDE_L_EN, LOW);
    sidePID.SetMode(MANUAL);
    stabilityPID.SetMode(MANUAL);
    sideOutput = 0;
    stabilityOutput = 0;
}
