#include "HeadSpinMotor.h"

HeadSpinMotor::HeadSpinMotor() {
    pinMode(PIN_HEAD_R_EN, OUTPUT);
    pinMode(PIN_HEAD_L_EN, OUTPUT);
}

HeadSpinMotor::~HeadSpinMotor() {}

void HeadSpinMotor::driveMotor(InputDevice* inputDevice, IMU imu) {
    if(!motorEnabled) {
        analogWrite(PIN_HEAD_R_PWM, 0);
        analogWrite(PIN_HEAD_L_PWM, 0);
        return;
    }

    short input = inputDevice->getHeadSpinInput();
    if(input > 0) {
        analogWrite(PIN_HEAD_R_PWM, input);
        analogWrite(PIN_HEAD_L_PWM, 0);
    } else if(input < 0) {
        analogWrite(PIN_HEAD_R_PWM, 0);
        analogWrite(PIN_HEAD_L_PWM, abs(input));
    } else {
        analogWrite(PIN_HEAD_R_PWM, 0);
        analogWrite(PIN_HEAD_L_PWM, 0);
    }
}

void HeadSpinMotor::enableMotor() {
    Motor::enableMotor();
    digitalWrite(PIN_HEAD_R_EN, HIGH);
    digitalWrite(PIN_HEAD_L_EN, HIGH);
}

void HeadSpinMotor::disableMotor() {
    Motor::disableMotor();
    digitalWrite(PIN_HEAD_R_EN, LOW);
    digitalWrite(PIN_HEAD_L_EN, LOW);
}
