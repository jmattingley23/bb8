#include "InputDevice.h"
#include "InputDeviceFactory.h"
#include "XboxController.h"
#include "Motor.h"
#include "MainDriveMotor.h"
#include "SideAxisMotor.h"
#include "FlywheelMotor.h"
#include "HeadSpinMotor.h"
#include "IMU.h"

#include <Arduino.h>

const byte NUM_MOTORS = 4;
const byte INTERRUPT_PIN = 19;

InputDevice *inputDevice;
Motor** motors;
IMU imu;
unsigned long startTime = 0;

void isr() {
    imu.dmpDataReady();
}

void setup() {
    Serial.begin(115200);
    #if !defined(__MIPSEL__)
        while (!Serial); // wait for serial port to connect
    #endif
    Serial.println(F("board serial ready"));

    TCCR2B = (TCCR2B & 0b11111000) | 0x01;
    TCCR4B = (TCCR4B & 0b11111000) | 0x01;

    imu.setup();

    Serial.println("imu setup done");

    pinMode(INTERRUPT_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, RISING);

    motors = new Motor*[NUM_MOTORS];
    
    motors[0] = new MainDriveMotor();
    motors[1] = new SideAxisMotor();
    motors[2] = new FlywheelMotor();
    motors[3] = new HeadSpinMotor();
    inputDevice = InputDeviceFactory::createInputDevice(InputDeviceFactory::InputDeviceType::Xbox, motors, NUM_MOTORS);
}

void loop() {
//    startTime = micros();
    inputDevice->gatherAllInputs();
    for(byte i = 0; i < NUM_MOTORS; i++) {
        motors[i]->driveMotor(inputDevice, imu);
    }
    imu.compute();
//    Serial.println(micros() - startTime);
}
