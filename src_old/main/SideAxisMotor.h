#ifndef SIDE_AXIS_MOTOR_H
#define SIDE_AXIS_MOTOR_H

#include "Motor.h"
#include "InputDevice.h"
#include "IMU.h"

#include <PID_v1.h>

class SideAxisMotor : public Motor {

public:
    SideAxisMotor();
    ~SideAxisMotor();
    void driveMotor(InputDevice* inputDevice, IMU imu);
    void enableMotor();
    void disableMotor();

private:
    const byte PIN_SIDE_R_PWM = 4;
    const byte PIN_SIDE_L_PWM = 5;

    const byte PIN_SIDE_R_EN = 46;
    const byte PIN_SIDE_L_EN = 47;

    const byte PIN_SIDE_POT = A15;
    const short SIDE_POT_MIN = 410;
    const short SIDE_POT_MAX = 600;

    const byte MIN_PWM = 00;

    double sideInput = 0;
    double sideOutput = 0;
    double sideSetpoint = 0;
    PID sidePID{&sideInput, &sideOutput, &sideSetpoint, 4.5, 0.0, 0.3, DIRECT};

    double stabilityInput = 0;
    double stabilityOutput = 0;
    double stabilitySetpoint = 0;
    PID stabilityPID{&stabilityInput, &stabilityOutput, &stabilitySetpoint, 2.8, 0.0, 0.0, REVERSE};
};

#endif
