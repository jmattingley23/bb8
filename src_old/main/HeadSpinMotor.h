#ifndef HEAD_SPIN_MOTOR_H
#define HEAD_SPIN_MOTOR_H

#include "Motor.h"
#include "InputDevice.h"
#include "IMU.h"

class HeadSpinMotor : public Motor {

public:
    HeadSpinMotor();
    ~HeadSpinMotor();
    void driveMotor(InputDevice* inputDevice, IMU imu);
    void enableMotor();
    void disableMotor();

private:
    const byte PIN_HEAD_R_PWM = 2;
    const byte PIN_HEAD_L_PWM = 3;

    const byte PIN_HEAD_R_EN = 42;
    const byte PIN_HEAD_L_EN = 43;

};

#endif
