#include "NRFInputDevice.h"

NRFInputDevice::NRFInputDevice(int pinCE, int pinCSN, boolean doAck) : radio(pinCE, pinCSN) {
    radio.begin();
    radio.setChannel(115); //above most 2.4ghz wifi networks
    radio.setPALevel(RF24_PA_MAX); //improve range
    radio.setDataRate(RF24_250KBPS); //improve range
    radio.openReadingPipe(0, address);
    if(!doAck) {
        radio.setAutoAck(false);
    }
    radio.startListening();
    delay(2); //without this delay the radio does not work
}

NRFInputDevice::~NRFInputDevice() {}

boolean NRFInputDevice::gatherAllInputs() {
    if(!radio.available()) {
        return false;
    }

    radio.read(&controllerData, sizeof(controllerData));

    return true;
}

void NRFInputDevice::printInputs() {
    Serial.println(controllerData.toString());
}

String NRFInputDevice::byteToPaddedString(byte buttons) {
    String ouput = "";
    String binaryStr = String(buttons, BIN);
    for(int i = 0; i < (8 - binaryStr.length()); i++) {
        ouput = String(ouput + "0");
    }
    return String(ouput + binaryStr);
}
