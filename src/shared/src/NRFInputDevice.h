#ifndef NRF_INPUT_DEVICE_H
#define NRF_INPUT_DEVICE_H

#include "InputDevice.h"
#include "shared/src/ControllerData.h"

#include <nRF24L01.h>
#include <RF24.h>

class NRFInputDevice : public InputDevice {

public:
    NRFInputDevice(int pinCE, int pinCSN, boolean doAck);
    ~NRFInputDevice();

    void printInputs();
    boolean gatherAllInputs();
    virtual void processAllInputs() = 0;

private:
    RF24 radio;
    const byte address[5] = {'0', 'x', 'B', 'B', '8'};
    boolean doAck;

protected:
    ControllerData::ControllerDataStruct controllerData;
    String byteToPaddedString(byte buttons);

};

#endif