#ifndef INPUT_DEVICE_H
#define INPUT_DEVICE_H

#include <Arduino.h>

class InputDevice {

public:
    InputDevice();
    virtual ~InputDevice() {};
    
    virtual void printInputs() = 0;
    virtual boolean gatherAllInputs() = 0;
    virtual void processAllInputs() = 0;
    virtual boolean toggleHoloProjector();
    virtual boolean playHappySound();
    virtual boolean playNeutralSound();
    virtual boolean playSadSound();
    virtual boolean toggleMotors();
    virtual int getThrottle();
    virtual int getHeadX();
    virtual int getHeadY();

};

#endif
