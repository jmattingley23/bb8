#include "SoundManager.h"

SoundManager::SoundManager() {
    for(int i = 0; i < numHappyPins; i++) {
        pinMode(happyPins[i], OUTPUT);
        digitalWrite(happyPins[i], HIGH);
    }
    for(int i = 0; i < numNeutralPins; i++) {
        pinMode(neutralPins[i], OUTPUT);
        digitalWrite(neutralPins[i], HIGH);
    }
    for(int i = 0; i < numSadPins; i++) {
        pinMode(sadPins[i], OUTPUT);
        digitalWrite(sadPins[i], HIGH);
    }

    randomSeed(analogRead(A1));
}

SoundManager::~SoundManager() {}

void SoundManager::playHappySound() {
    long randIndex = random(numHappyPins);
    digitalWrite(happyPins[randIndex], LOW);
}

void SoundManager::playNeutralSound() {
    long randIndex = random(numNeutralPins);
    digitalWrite(neutralPins[randIndex], LOW);
}

void SoundManager::playSadSound() {
    long randIndex = random(numSadPins);
    digitalWrite(sadPins[randIndex], LOW);
}

void SoundManager::stopHappySound() {
    for(int i = 0; i < numHappyPins; i++) {
        digitalWrite(happyPins[i], HIGH);
    }
}

void SoundManager::stopNeutralSound() {
    for(int i = 0; i < numNeutralPins; i++) {
        digitalWrite(neutralPins[i], HIGH);
    }
}

void SoundManager::stopSadSound() {
    for(int i = 0; i < numSadPins; i++) {
        digitalWrite(sadPins[i], HIGH);
    }
}