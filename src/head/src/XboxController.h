#ifndef XBOX_CONTROLLER_H
#define XBOX_CONTROLLER_H

#include "shared/src/NRFInputDevice.h"

class XboxController : public NRFInputDevice {

public:
    XboxController(int pinCE, int pinCSN, boolean doAckPayload);
    ~XboxController();
    boolean gatherAllInputs();
    void processAllInputs();

    boolean toggleHoloProjector();
    boolean playHappySound();
    boolean playNeutralSound();
    boolean playSadSound();

private:
    String buttonStr;

    boolean dPadRightPressed = false;
    boolean dPadRightPressedPrev = false;
    boolean dPadLeftPressed = false;
    boolean dPadLeftPressedPrev = false;
    boolean dPadDownPressed = false;
    boolean dPadDownPressedPrev = false;
    boolean dPadUpPressed = false;
    boolean dPadUpPressedPrev = false;

};

#endif
