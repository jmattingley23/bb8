#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include "Arduino.h"

class SoundManager {

public:
    SoundManager();
    ~SoundManager();

    void playHappySound();
    void playNeutralSound();
    void playSadSound();

    void stopHappySound();
    void stopNeutralSound();
    void stopSadSound();

private:
    byte happyPins[1] = {6};
    byte neutralPins[3] = {7, 2, 3};
    byte sadPins[1] = {4}; // & 5

    byte numHappyPins = (sizeof(happyPins)/sizeof(happyPins[0]));
    byte numNeutralPins = (sizeof(neutralPins)/sizeof(neutralPins[0]));
    byte numSadPins = (sizeof(sadPins)/sizeof(sadPins[0]));

};

#endif