#include "shared/src/InputDevice.h"
#include "HeadInputDeviceFactory.h"
#include "LEDManager.h"
#include "SoundManager.h"

#include <Arduino.h>

const float LOOP_INTERVAL = 16.666;  //60hz in ms
long previousMillis = 0;

void processSounds();
void processLEDs();

InputDevice* inputDevice;
LEDManager ledManager;
SoundManager soundManager;

void setup() {
    Serial.begin(9600);
    Serial.println("Head serial ready");
    inputDevice = HeadInputDeviceFactory::createInputDevice(HeadInputDeviceFactory::HeadInputDeviceType::Xbox);
    ledManager.enableStaticLeds();
}

void loop() {
    if(!inputDevice->gatherAllInputs()) {
        // return; //do we need this on the head?  sounds could potentially get stuck on loop if the transmitter gets disconnected before the button is unpressed
        // also, look into disable all acks in the head (auto and otherwise)
    }
    inputDevice->processAllInputs();

    processLEDs();
    processSounds();
}

void processLEDs() {
    if(inputDevice->toggleHoloProjector()) {
        ledManager.toggleHolorProjector();
    }
    ledManager.doPSI();
}

void processSounds() {
    inputDevice->playHappySound() ? soundManager.playHappySound() : soundManager.stopHappySound();

    inputDevice->playNeutralSound() ? soundManager.playNeutralSound() : soundManager.stopNeutralSound();

    inputDevice->playSadSound() ? soundManager.playSadSound() : soundManager.stopSadSound();
}