#include "XboxController.h"

XboxController::XboxController(int pinCE, int pinCSN, boolean doAckPayload) : NRFInputDevice(pinCE, pinCSN, doAckPayload) {}

XboxController::~XboxController() {}

boolean XboxController::gatherAllInputs() {
    if(!NRFInputDevice::gatherAllInputs()) {
        return false;
    }
    
    buttonStr = byteToPaddedString(controllerData.buttons);
    return true;
}

void XboxController::processAllInputs() {
    dPadRightPressedPrev = dPadRightPressed;
    dPadRightPressed = (buttonStr.charAt(7) == '1');

    // dPadLeftPressedPrev = dPadLeftPressed;
    dPadLeftPressed = (buttonStr.charAt(6) == '1');

    // dPadDownPressedPrev = dPadDownPressed;
    dPadDownPressed = (buttonStr.charAt(5) == '1');

    // dPadUpPressedPrev = dPadUpPressed;
    dPadUpPressed = (buttonStr.charAt(4) == '1');
}

boolean XboxController::toggleHoloProjector() {
    return (!dPadRightPressedPrev && dPadRightPressed);
}

boolean XboxController::playHappySound() {
    return dPadUpPressed;
    // return (!dPadUpPressedPrev && dPadUpPressed);
}
boolean XboxController::playNeutralSound() {
    return dPadLeftPressed;
    // return (!dPadLeftPressedPrev && dPadLeftPressed);
}
boolean XboxController::playSadSound() {
    return dPadDownPressed;
    // return (!dPadDownPressedPrev && dPadDownPressed);
}