#ifndef LED_MANAGER_H
#define LED_MANAGER_H

#include "shared/src/Colors.h"

#include <Adafruit_NeoPixel.h>

class LEDManager {
public:
    LEDManager();
    ~LEDManager();
    void enableStaticLeds();
    void toggleHolorProjector();
    void doPSI();

private:
    Adafruit_NeoPixel pixels = Adafruit_NeoPixel(9, 10, NEO_GRB + NEO_KHZ800);
    void setLED(byte led, const byte channels[3]);
    void enableHoloProjector();
    void disableHoloProjector();
    void enablePSI();
    void disablePSI();

    boolean holoProjectorEnabled = false;
    const byte PIN_AUDIO_IN = A0;
    const int PSI_THRESHOLD = 15;
    const byte PSI_LED_INDEX = 8;
    const byte RADAR_LED_INDEX = 7;

};

#endif