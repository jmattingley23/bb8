#ifndef HEAD_INPUT_DEVICE_FACTORY_H
#define HEAD_INPUT_DEVICE_FACTORY_H

#include "XboxController.h"

#include "shared/src/InputDevice.h"

class HeadInputDeviceFactory {

public:
    enum HeadInputDeviceType{Xbox};
    static InputDevice* createInputDevice(HeadInputDeviceType type);

private:
    const static int PIN_CE = 8;
    const static int PIN_CSN = 9;
    const static boolean DO_ACK_PAYLOAD = false;

};

#endif
