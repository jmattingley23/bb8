#include "LEDManager.h"

LEDManager::LEDManager() {
    pixels.begin();
    pixels.clear();
    pinMode(PIN_AUDIO_IN, INPUT);
}

LEDManager::~LEDManager() {}

void LEDManager::enableStaticLeds() {
    setLED(RADAR_LED_INDEX, Colors::getInstance().RED);
}

void LEDManager::toggleHolorProjector() {
    if(holoProjectorEnabled) {
        disableHoloProjector();
        holoProjectorEnabled = false;
    } else {
        enableHoloProjector();
        holoProjectorEnabled = true;
    }
}

void LEDManager::doPSI() {
    int audioIn = analogRead(PIN_AUDIO_IN);
    if(audioIn > PSI_THRESHOLD) {
        enablePSI();
    } else {
        disablePSI();
    }
}

void LEDManager::enableHoloProjector() {
    for(int i = 0; i < 7; i++) {
        setLED(i, Colors::getInstance().WHITE);
    }
}

void LEDManager::disableHoloProjector() {
    for(int i = 0; i < 7; i++) {
        setLED(i, Colors::getInstance().OFF);
    }
}

void LEDManager::enablePSI() {
    setLED(PSI_LED_INDEX, Colors::getInstance().WHITE);
}

void LEDManager::disablePSI() {
    setLED(PSI_LED_INDEX, Colors::getInstance().OFF);
}

void LEDManager::setLED(byte led, const byte channels[3]) {
    pixels.setPixelColor(led, pixels.Color(channels[0], channels[1], channels[2]));
    pixels.show();
}