#include "HeadInputDeviceFactory.h"

InputDevice* HeadInputDeviceFactory::createInputDevice(HeadInputDeviceType type) {
    if(type == HeadInputDeviceFactory::HeadInputDeviceType::Xbox) {
        return new XboxController(PIN_CE, PIN_CSN, DO_ACK_PAYLOAD);
    }
    return NULL;
}
