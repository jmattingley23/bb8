#ifndef XBOX_CONTROLLER_H
#define XBOX_CONTROLLER_H

#include "shared/src/NRFInputDevice.h"

class XboxController : public NRFInputDevice {

public:
    XboxController(int pinCE, int pinCSN, boolean doAckPayload);
    ~XboxController();
    boolean gatherAllInputs();
    void processAllInputs();

    boolean toggleMotors();
    int getThrottle();
    int getHeadX();
    int getHeadY();

private:
    String buttonStr;

    boolean startPressed = false;
    boolean startPressedPrev = false;

};

#endif