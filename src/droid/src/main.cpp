#include "shared/src/InputDevice.h"
#include "DroidInputDeviceFactory.h"
#include "IMU.h"
#include "Motor.h"
#include "motors/MainDriveMotor.h"
#include "motors/HeadServos.h"

#include <Arduino.h>
#include <avr/wdt.h>

const float LOOP_INTERVAL = 5;  //200Hz in ms; could go higher but max sample rate of IMU is 200hz
long previousMillis = 0;
volatile boolean shutdownFlag = false;
volatile boolean connectedOnce = false;
boolean motorsEnabled = false;

// void initWDT(byte sWDT, byte time);
// void doGracefulShutdown();
// void reset();

const byte NUM_MOTORS = 2;

InputDevice* inputDevice;
Motor** motors;

long startTime = 0;


void setup() {
    Serial.begin(9600);
    // wdt_disable();
    // while(!Serial);
    Serial.println("Droid serial ready");
    IMU::init();
    inputDevice = DroidInputDeviceFactory::createInputDevice(DroidInputDeviceFactory::DroidInputDeviceType::Xbox);

    motors = new Motor*[NUM_MOTORS];
    motors[0] = new MainDriveMotor(inputDevice);
    motors[1] = new HeadServos(inputDevice);
    // initWDT(0b01000000, WDTO_1S); //intrerrupt, no reset
}

void loop() {
    startTime = millis();
    if(shutdownFlag) {
        // doGracefulShutdown();
        return;
    }
    if((millis() - previousMillis) < LOOP_INTERVAL) {
        return;
    }

    inputDevice->gatherAllInputs();
    // if(!inputDevice->gatherAllInputs()) {
    //     return;
    // }


    IMU::gatherSensorData();
    inputDevice->processAllInputs();
    // Serial.print(IMU::getPitch());
    // Serial.print(" ");
    // Serial.println(IMU::getRoll());
    // inputDevice->printInputs();
    connectedOnce = true;
    previousMillis = millis();
    // Serial.println(previousMillis);
    wdt_reset();

    if(inputDevice->toggleMotors()) {
        motorsEnabled = !motorsEnabled;
    }

    // Serial.println(motorsEnabled);

    for(byte i = 0; i < NUM_MOTORS; i++) {
        if(motorsEnabled) {
            motors[i]->enableMotor();
        } else {
            motors[i]->disableMotor();
        }
        motors[i]->driveMotor();
    }

    // Serial.println(millis() - startTime);
}

// void initWDT(byte sWDT, byte time) {
//     WDTCSR |= 0b00011000;
//     WDTCSR = sWDT | time;
//     wdt_reset();
// }

// ISR (WDT_vect) {
//     Serial.println("watchdog timer expired, setting flag...");
//     shutdownFlag = true;
//     Serial.println("done");
// }

// void doGracefulShutdown() {
//     if(!connectedOnce) {
//         shutdownFlag = false;
//         return;
//     }
//     wdt_disable();
//     Serial.println("performing graceful shutdown...");
//     delay(5000);
//     Serial.println("shutdown completed. restarting...");
//     reset();
// }

// void reset() {
//     initWDT(0b00001000, WDTO_30MS);
//     while(1) {}
// }