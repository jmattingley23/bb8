#include "IMU.h"

MPU6050 IMU::mpu;
bool IMU::dmpReady = false;
volatile bool IMU::mpuInterrupt = false;
uint8_t IMU::devStatus = 0;
uint8_t IMU::fifoBuffer[64];
Quaternion IMU::q;
VectorFloat IMU::gravity;
float IMU::ypr[3];
float IMU::pitch = 0;
float IMU::roll = 0;


IMU::IMU() {}
IMU::~IMU() {}

void IMU::dmpDataReady() {
    mpuInterrupt = true;
}

void IMU::init() {
    Wire2.begin();
    Wire2.setClock(400000); // 400kHz I2C clock

    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    // verify
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
    devStatus = mpu.dmpInitialize();
    dmpReady = false;
    mpuInterrupt = false;

    // calibrated using IMUZero
    mpu.setXGyroOffset(122);
    mpu.setYGyroOffset(-31);
    mpu.setZGyroOffset(3);
    mpu.setXAccelOffset(-2540);
    mpu.setYAccelOffset(-941);
    mpu.setZAccelOffset(1247);

    if (devStatus == 0) {
        mpu.PrintActiveOffsets();
        // turn on the DMP, now that it's ready
        Serial.print(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);
        Serial.println(F("Done"));

        // enable Arduino interrupt detection
        Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        Serial.println(F("Done"));

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // let data stabilize
        delay(1000);
    } else {
        Serial.println(F("DMP Initialization failed!"));
        if(devStatus == 1) {
            Serial.println(F("Memory load failed"));
        } else if(devStatus == 2) {
            Serial.println(F("DMP configuration failed"));
        }
    }
}

void IMU::gatherSensorData() {
    // do nothing if setup failed
    if(!dmpReady) {
        return;
    }

    if (mpu.dmpGetCurrentFIFOPacket(fifoBuffer)) { // Get the Latest packet
        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
        pitch = (ypr[1] * 180 / M_PI);
        roll = (ypr[2] * 180 / M_PI);
    }
}

float IMU::getPitch() {
    return pitch;
}

float IMU::getRoll() {
    return roll;
}