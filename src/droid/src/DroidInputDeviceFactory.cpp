#include "DroidInputDeviceFactory.h"

InputDevice* DroidInputDeviceFactory::createInputDevice(DroidInputDeviceType type) {
    if(type == DroidInputDeviceFactory::DroidInputDeviceType::Xbox) {
        return new XboxController(PIN_CE, PIN_CSN, DO_ACK);
    }
    return NULL;
}
