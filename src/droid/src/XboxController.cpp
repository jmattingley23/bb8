#include "XboxController.h"

XboxController::XboxController(int pinCE, int pinCSN, boolean doAckPayload) : NRFInputDevice(pinCE, pinCSN, doAckPayload) {}

XboxController::~XboxController() {}

boolean XboxController::gatherAllInputs() {
    if(!NRFInputDevice::gatherAllInputs()) {
        return false;
    }

    buttonStr = byteToPaddedString(controllerData.buttons);
    return true;
}

void XboxController::processAllInputs() {
    startPressedPrev = startPressed;
    startPressed = (buttonStr.charAt(3) == '1');
}

boolean XboxController::toggleMotors() {
    return (!startPressedPrev && startPressed);
}

int XboxController::getThrottle() {
    return controllerData.triggers;
}

int XboxController::getHeadX() {
    return controllerData.rightX;
}

int XboxController::getHeadY() {
    return controllerData.rightY;
}
