#ifndef DROID_INPUT_DEVICE_FACTORY_H
#define DROID_INPUT_DEVICE_FACTORY_H

#include "XboxController.h"

#include "shared/src/InputDevice.h"

class DroidInputDeviceFactory {

public:
    enum DroidInputDeviceType{Xbox};
    static InputDevice* createInputDevice(DroidInputDeviceType type);

private:
    const static int PIN_CE = 35;
    const static int PIN_CSN = 34;
    const static boolean DO_ACK = true;
    
};

#endif
