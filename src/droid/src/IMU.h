#ifndef IMU_H
#define IMU_H

#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps_V6_12.h"
#include "Wire.h"

class IMU {

#define INTERRUPT_PIN 13

public:
    static void init();
    static void gatherSensorData();
    static float getPitch();
    static float getRoll();
    

private:
    IMU();
    ~IMU();
    static void dmpDataReady();

    static MPU6050 mpu;

    // MPU control/status vars
    static bool dmpReady;               // set true if DMP init was successful
    static volatile bool mpuInterrupt;  // set true if new data is available in DMP FIFO buffer
    static uint8_t devStatus;           // return status after each device operation (0 = success, !0 = error)
    static uint8_t fifoBuffer[64];      // FIFO storage buffer

    // orientation/motion vars
    static Quaternion q;           // [w, x, y, z]         quaternion container
    static VectorFloat gravity;    // [x, y, z]            gravity vector
    static float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
    static float pitch;
    static float roll;
};

#endif
