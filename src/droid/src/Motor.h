#ifndef MOTOR_H
#define MOTOR_H

#include "IMU.h"
#include "shared/src/InputDevice.h"

#include <Arduino.h>

class Motor { 

public:
    Motor(InputDevice* inputDevice);
    ~Motor();

    virtual void driveMotor() = 0;
    virtual void enableMotor() = 0;
    virtual void disableMotor() = 0;

protected:
    InputDevice* inputDevice;

};

#endif
