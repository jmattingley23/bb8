#include "MainDriveMotor.h"

MainDriveMotor::MainDriveMotor(InputDevice* inputDevice) : Motor(inputDevice) {}

MainDriveMotor::~MainDriveMotor() {}

void MainDriveMotor::driveMotor() {
    throttle = inputDevice->getThrottle();

    if(throttle > 0) {
        analogWrite(PIN_R_PWM, throttle);
        analogWrite(PIN_L_PWM, 0);
    } else if (throttle < 0) {
        analogWrite(PIN_R_PWM, 0);
        analogWrite(PIN_L_PWM, abs(throttle));
    } else {
        analogWrite(PIN_R_PWM, 0);
        analogWrite(PIN_L_PWM, 0);
    }
}

void MainDriveMotor::enableMotor() {
    digitalWrite(PIN_R_EN, HIGH);
    digitalWrite(PIN_L_EN, HIGH);
}

void MainDriveMotor::disableMotor() {
    digitalWrite(PIN_R_EN, LOW);
    digitalWrite(PIN_L_EN, LOW);
}