#include "HeadServos.h"

HeadServos::HeadServos(InputDevice* inputDevice) : Motor(inputDevice) {
    stickX = 0;
    stickY = 0;
    leftPosition = 0;
    rightPosition = 0;
    sideDiff = 0;

}

HeadServos::~HeadServos() {}

void HeadServos::driveMotor() {
    //gather inputs
    stickX = inputDevice->getHeadX();
    stickY = inputDevice->getHeadY();

    //map stick Y value to servo Y min/max
    if(stickY < 0) {
        rightPosition = map(stickY, -255, 0, SERVO_MIN_Y, SERVO_MID_Y);
    } else if(stickY > 0) {
        rightPosition = map(stickY, 0, 255, SERVO_MID_Y, SERVO_MAX_Y);
    } else {
        rightPosition = SERVO_MID_Y;
    }
    leftPosition = rightPosition;

    //map stick X value to servo X min/max
    if(stickX < 0) {
        sideDiff = map(stickX, -255, 0, (-1 * SERVO_MAX_X), 0);
    } else if(stickX > 0) {
        sideDiff = map(stickX, 0, 255, 0, SERVO_MAX_X);
    } else {
        sideDiff = 0;
    }
    leftPosition -= sideDiff;
    rightPosition += sideDiff;

    //left servo is mounted backwards so let's flip it
    leftPosition = map(leftPosition, 0, 180, 180, 0);

    //sanity check
    leftPosition = constrain(leftPosition, 0, 180);
    rightPosition = constrain(rightPosition, 0, 180);

    //write values to motors
    servoLeft.write(leftPosition);
    servoRight.write(rightPosition);
}

void HeadServos::enableMotor() {
    servoLeft.attach(PIN_SERVO_L);
    servoRight.attach(PIN_SERVO_R);
}

void HeadServos::disableMotor() {
    servoLeft.detach();
    servoRight.detach();
}