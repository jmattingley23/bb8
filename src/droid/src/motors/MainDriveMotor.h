#include "../Motor.h"

class MainDriveMotor : public Motor {

public:
    MainDriveMotor(InputDevice* inputDevice);
    ~MainDriveMotor();

    void driveMotor();
    void enableMotor();
    void disableMotor();

private:
    const byte PIN_R_PWM = 3;
    const byte PIN_L_PWM = 2;

    const byte PIN_R_EN = 16;
    const byte PIN_L_EN = 17;

    int throttle = 0;
};