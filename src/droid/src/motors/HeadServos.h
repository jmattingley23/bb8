#include "../Motor.h"

#include <Servo.h>

class HeadServos : public Motor {

public:
    HeadServos(InputDevice* InputDevice);
    ~HeadServos();

    void driveMotor();
    void enableMotor();
    void disableMotor();

private:
    const byte PIN_SERVO_R = 36;
    const byte PIN_SERVO_L = 37;

    const byte SERVO_MAX_Y = 65;
    const byte SERVO_MID_Y = 140;
    const byte SERVO_MIN_Y = 170;
    const byte SERVO_MAX_X = 11;

    int stickX;
    int stickY;
    byte leftPosition;
    byte rightPosition;
    int sideDiff;

    Servo servoLeft;
    Servo servoRight;

};