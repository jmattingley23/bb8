#include <Arduino.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Adafruit_NeoPixel.h>

#include "Controller.h"
#include "shared/src/Colors.h"
#include "shared/src/ControllerData.h"

RF24 radio(9, 10);  // CE, CSN
// RF24 radio(8, 9);  // CE, CSN
const byte address[5] = {'0', 'x', 'B', 'B', '8'};

void setLED(const byte channels[3]);
boolean writeIncreasingPower(const void *buf, uint8_t len);

const float LOOP_INTERVAL = 16.666;  //60Hz in ms
const short FAIL_INTERVAL = 250;  //250 ms
long previousMillis = 0;
long lastConnected = 0;
boolean writeStatus = false;

const byte LED_PIN = 5;

Adafruit_NeoPixel pixel = Adafruit_NeoPixel(1, LED_PIN, NEO_GRB + NEO_KHZ800);

Controller controller;
ControllerData::ControllerDataStruct controllerData;

uint8_t PA_LEVELS[2] = { RF24_PA_MIN, RF24_PA_MAX };

void setup() {
    Serial.begin(9600);
    pixel.begin();
    radio.begin();
    radio.setChannel(115); //above most 2.4ghz wifi networks
    radio.setPALevel(RF24_PA_MIN); //start low for nearby receivers
    radio.setDataRate(RF24_250KBPS); //improve range
    radio.openWritingPipe(address);
    radio.setRetries(3, 3); //1ms delay, 3 retries
    radio.stopListening();
    setLED(Colors::getInstance().RED);
    TCCR2B = (TCCR2B & 0b11111000) | 0x01; //adjust PWM frequency for trigger motor whine
    Serial.println("starting up...");
}

void loop() {
    long startTime = millis();
    if((millis() - previousMillis) < LOOP_INTERVAL) {
        return;
    }
    previousMillis = millis();

    controller.gatherAllInputs();
    controller.doRumbleMotors();
    controllerData = controller.getStruct();

    writeStatus = writeIncreasingPower(&controllerData, sizeof(controllerData));

    if(writeStatus == 1) {
        lastConnected = millis();
        setLED(Colors::getInstance().GREEN);
    } else {
        // if((millis() - lastConnected) >= FAIL_INTERVAL) {
            setLED(Colors::getInstance().RED);
        // }
    }
    long endTime = millis() - startTime;

    // Serial.println(endTime);
}

void setLED(const byte channels[3]) {
    pixel.setPixelColor(0, pixel.Color(channels[0], channels[1], channels[2]));
    pixel.show();
}

boolean writeIncreasingPower(const void *buf, uint8_t len) {
    for(uint8_t i = 0; i < sizeof(PA_LEVELS) / sizeof(PA_LEVELS[0]); i++) {
        radio.setPALevel(PA_LEVELS[i]);
        if(radio.write(buf, len)) {
            return true;
        }
    }
    return false;
}