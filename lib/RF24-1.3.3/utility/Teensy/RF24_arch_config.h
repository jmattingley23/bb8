

  #if ARDUINO < 100
	#include <WProgram.h>
  #else
	#include <Arduino.h>
  #endif

  #include <stddef.h>
  
  #include <stdint.h>
  #include <stdio.h>
  #include <string.h>

  #include <SPI.h>
  #ifdef NRF_SPI_BUS
    #if NRF_SPI_BUS == 1
      #define _SPI SPI1
    #else
      #define _SPI SPI
    #endif
  #else
    #define _SPI SPI
  #endif

  #define printf Serial.printf
  
  #ifdef SERIAL_DEBUG
	#define IF_SERIAL_DEBUG(x) ({x;})
  #else
	#define IF_SERIAL_DEBUG(x)
  #endif
  

  #define PRIPSTR "%s"




